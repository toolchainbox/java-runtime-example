FROM tomcat:9.0

ADD https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war /usr/local/tomcat/webapps/

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y autoremove && apt-get clean \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && chmod -R 777 /usr/local/tomcat/

EXPOSE 8080

# NOTE: Open via http://host/sample/
